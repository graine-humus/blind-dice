/** @type {import('tailwindcss').Config} */
export default {
	content: ['./src/**/*.{html,js,svelte,ts}'],
	theme: {
		fontFamily: {
			sans: ['Tahoma', 'Calibri', 'Helvetica', 'Arial', 'Verdana', 'Times New Roman']
		},
		extend: {}
	},
	plugins: []
};
