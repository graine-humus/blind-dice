import { locale, getLocaleFromNavigator, addMessages } from 'svelte-i18n';

import en from '../locales/en.json';
import fr from '../locales/fr.json';

addMessages('en', en);
addMessages('fr', fr);
locale.set(getLanguageLocale());

function getLanguageLocale() {
	const browserLang = getLocaleFromNavigator() || 'en-US';
	return browserLang.split('-')[0];
}
